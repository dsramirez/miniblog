package com.mitocode.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mitocode.dao.IUsuarioDAO;
import com.mitocode.dao.impl.UsuarioDAOImpl;
import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;
import com.mitocode.service.impl.UsuarioServiceImpl;

import org.mindrot.jbcrypt.BCrypt;




/**
 * @author david
 *
 */
@Named
@ViewScoped
public class UsuarioBean implements Serializable {
	@Inject
	private IUsuarioService service;
	@Inject
	private IUsuarioDAO dao;
	
	
/*@Inject
	private UsuarioServiceImpl service1;*/
	
	private List<Usuario> listaUsuario;
	private List<Usuario> listaUsuarioFiltered;
    private String claveActual;
    private Boolean verificado;
    private String clavenueva;
    private String claveConfirmada;
    
    
	
	
	@PostConstruct
	public void init() {
		listarUsuario();
		verificado=false;
	}
	
	
	public List<Usuario> getListaUsuario() {
		return listaUsuario;
	}


	public void setListaUsuario(List<Usuario> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}


	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}


	private Usuario usuario;
	
	
	public void listarUsuario() {
		try {
			listaUsuario=this.service.listar();
			
		} catch (Exception e) {
			
		}
	}


	public List<Usuario> getListaUsuarioFiltered() {
		return listaUsuarioFiltered;
	}


	public void setListaUsuarioFiltered(List<Usuario> listaUsuarioFiltered) {
		this.listaUsuarioFiltered = listaUsuarioFiltered;
	}


	public String getClaveActual() {
		return claveActual;
	}


	public void setClaveActual(String claveActual) {
		this.claveActual = claveActual;
	}
	
	public Boolean getVerificado() {
		return verificado;
	}


	public void setVerificado(Boolean verificado) {
		this.verificado = verificado;
	}
	
	
	
	
	public String getClavenueva() {
		return clavenueva;
	}


	public void setClavenueva(String clavenueva) {
		this.clavenueva = clavenueva;
	}


	public String getClaveConfirmada() {
		return claveConfirmada;
	}


	public void setClaveConfirmada(String claveConfirmada) {
		this.claveConfirmada = claveConfirmada;
	}
	
	
	
	 
	public boolean verifica()
	{
		boolean password_verified = false;
		password_verified = BCrypt.checkpw(claveActual, dao.traerPassHashed(usuario.getUsuario()));
		if (password_verified) {
			verificado=true;
		}else {
			verificado=false;
		}
		
		return dao.verificarUsuario(claveActual, usuario.getUsuario());
	}


	
	public String modificar() throws Exception {
		if (clavenueva.equalsIgnoreCase(claveConfirmada)) {
			String newclave = BCrypt.hashpw(clavenueva, BCrypt.gensalt());
			usuario.setContrasena(newclave);
			
		int resu=	service.modificar(usuario);
		System.out.println(resu+"resu");
		System.out.println(clavenueva+"clavenueva");
		
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Modificacion de clave con exito"));
			
		}else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "Credenciales no concuerdan"));
			
		}
		
		
		return null;
	}

}
