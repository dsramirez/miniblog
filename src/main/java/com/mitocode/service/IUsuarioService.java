package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Usuario;

public interface IUsuarioService {

	Usuario login(Usuario us);
	
	
	Integer modificar(Usuario t) throws Exception;
	
	List<Usuario> listar() throws Exception;
	
	
}
