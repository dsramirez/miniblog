package com.mitocode.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import org.mindrot.jbcrypt.BCrypt;

import com.mitocode.dao.IUsuarioDAO;
import com.mitocode.dao.impl.UsuarioDAOImpl;
import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;

@Named
public class UsuarioServiceImpl implements IUsuarioService, Serializable {

	@EJB
	private IUsuarioDAO dao;
	/*@EJB
	private UsuarioDAOImpl dao1;*/
	

	@Override
	public Usuario login(Usuario us) {
		String clave = us.getContrasena();
		String claveHash = dao.traerPassHashed(us.getUsuario());

		try {
			if (BCrypt.checkpw(clave, claveHash)) {
				return dao.leerPorNombreUsuario(us.getUsuario());
			}
		} catch (Exception e) {
			throw e;
		}

		return new Usuario();
	}

	@Override
	public Integer modificar(Usuario t) throws Exception {
		int flag = 0;

		try {

			flag = 1;
			dao.modificar(t);

		} catch (Exception e) {
			e.printStackTrace();
			flag = 0;
		}

		return flag;
	}

	@Override
	public List<Usuario> listar() throws Exception {

		return dao.listar();
	}
	
	
	/*
	public List<Usuario> filtroPantalla(String usr){
		return dao1.filtroUsuario(usr);
	}*/

}
